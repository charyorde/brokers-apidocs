swagger: '2.0'
info:
  x-ibm-name: likid-api
  title: Likid
  version: 1.0.0
  description: Likid API provides infrastructure APIs to financial services. Likid is backed by services rendered by a network of financial institutions.
schemes:
  - https
host: api.huntrecht.com
basePath: /v1
consumes:
  - application/json
produces:
  - application/json
securityDefinitions: {}
x-ibm-configuration:
  testable: true
  enforced: true
  cors:
    enabled: true
  assembly:
    execute:
      - switch:
          title: switch
          case:
            - condition: (api.operation.id==='Server Info')
              execute:
                - invoke:
                    target-url: 'https://brokers-api.huntrecht-live.eu-gb.containers.appdomain.cloud$(request.path)'
                    verb: GET
                    title: info
            - condition: (api.operation.id==='ListProducts')
              execute:
                - invoke:
                    title: listProducts
                    timeout: 60
                    verb: GET
                    cache-response: protocol
                    cache-ttl: 900
                    stop-on-error: []
                    version: 1.0.0
                    target-url: 'https://brokers-api.huntrecht-live.eu-gb.containers.appdomain.cloud$(request.path)'
            - condition: (api.operation.id==='AddUser')
              execute:
                - invoke:
                    title: addUser
                    timeout: 60
                    verb: POST
                    cache-response: protocol
                    cache-ttl: 900
                    stop-on-error: []
                    version: 1.0.0
                    target-url: 'https://brokers-api.huntrecht-live.eu-gb.containers.appdomain.cloud$(request.path)'
            - condition: (api.operation.id==='CreateAccount')
              execute:
                - invoke:
                    title: createAccount
                    timeout: 60
                    verb: POST
                    cache-response: protocol
                    cache-ttl: 900
                    stop-on-error: []
                    version: 1.0.0
                    target-url: 'https://brokers-api.huntrecht-live.eu-gb.containers.appdomain.cloud$(request.path)'
            - condition: (api.operation.id==='Authorize')
              execute:
                - invoke:
                    title: authorize
                    timeout: 60
                    verb: POST
                    cache-response: protocol
                    cache-ttl: 900
                    stop-on-error: []
                    version: 1.0.0
                    target-url: 'https://brokers-api.huntrecht-live.eu-gb.containers.appdomain.cloud$(request.path)'
            - condition: (api.operation.id==='Authenticate')
              execute:
                - invoke:
                    title: authenticate
                    timeout: 60
                    verb: POST
                    cache-response: protocol
                    cache-ttl: 900
                    stop-on-error: []
                    version: 1.0.0
                    target-url: 'https://brokers-api.huntrecht-live.eu-gb.containers.appdomain.cloud$(request.path)'
            - condition: (api.operation.id==='CreditRequest')
              execute:
                - invoke:
                    title: creditRequest
                    timeout: 60
                    verb: POST
                    cache-response: protocol
                    cache-ttl: 900
                    stop-on-error: []
                    version: 1.0.0
                    target-url: 'https://brokers-api.huntrecht-live.eu-gb.containers.appdomain.cloud$(request.path)'
            - condition: (api.operation.id==='CreditScoreCard')
              execute:
                - invoke:
                    title: loadCreditScoreCard
                    timeout: 60
                    verb: GET
                    cache-response: protocol
                    cache-ttl: 900
                    stop-on-error: []
                    version: 1.0.0
                    target-url: 'https://brokers-api.huntrecht-live.eu-gb.containers.appdomain.cloud$(request.path)'
            - condition: (api.operation.id==='VerifyUser')
              execute:
                - invoke:
                    title: verifyUser
                    timeout: 60
                    verb: PUT
                    cache-response: protocol
                    cache-ttl: 900
                    stop-on-error: []
                    version: 1.0.0
                    target-url: 'https://brokers-api.huntrecht-live.eu-gb.containers.appdomain.cloud$(request.path)'
          version: 1.0.0
  phase: realized
paths:
  /info:
    get:
      responses:
        '200':
          description: 200 OK
      tags:
        - Server Information
      description: Get basic information about the Likid API
      operationId: Server Info
      summary: Server Info
  /funds:
    get:
      responses:
        '200':
          description: 200 OK
      tags:
        - Funds
      operationId: ListFunds
      summary: List funds
    post:
      responses:
        '200':
          description: 200 OK
      tags:
        - Funds
      summary: Create new fund
      parameters:
        - name: fund
          required: true
          in: body
          schema:
            $ref: '#/definitions/Fund'
          description: The fund object
      operationId: CreateFund
  /kyc:
    get:
      responses:
        '200':
          description: 200 OK
      tags:
        - Kyc
      operationId: ListKyc
      description: List all Kyc
    post:
      responses:
        '200':
          description: 200 OK
          schema:
            $ref: '#/definitions/Kyc'
        '401':
          description: Unauthorized
          schema:
            type: object
      tags:
        - Kyc
      parameters:
        - name: kyc
          required: true
          in: body
          schema:
            $ref: '#/definitions/Kyc'
      operationId: CreateKyc
  /withdraw:
    post:
      responses:
        '200':
          description: 200 OK
          schema:
            $ref: '#/definitions/Withdraw'
      tags:
        - Withdraw
      parameters:
        - name: withdraw
          required: true
          in: body
          schema:
            $ref: '#/definitions/Withdraw'
      operationId: CreateNewWithdrawal
  /transfers:
    get:
      responses:
        '200':
          description: 200 OK
      description: List own transfers
      tags:
        - Transfers
      operationId: ListTransfers
      summary: List Transfers
    post:
      responses:
        '200':
          description: 200 OK
          schema:
            $ref: '#/definitions/Transfer'
      description: Create new transfers
      tags:
        - Transfers
      parameters:
        - name: transfer
          required: true
          in: body
          schema:
            $ref: '#/definitions/Transfer'
      summary: User1 must own something before it can be transferred to User2
      operationId: Create
  '/funds/{id}':
    get:
      responses:
        '200':
          description: 200 OK
      summary: ''
      operationId: GetFundById
      parameters:
        - name: id
          type: string
          required: true
          in: path
          description: The fund id
      tags:
        - Funds
  '/transfers/{id}':
    get:
      responses:
        '200':
          description: 200 OK
      operationId: GetTransferById
      parameters:
        - name: id
          type: string
          required: true
          in: path
      tags:
        - Transfers
  '/kyc/{id}':
    get:
      responses:
        '200':
          description: 200 OK
      operationId: GetKycById
      summary: Get a KYC
      tags:
        - Kyc
      parameters:
        - name: id
          type: string
          required: true
          in: path
      description: Get a KYC
  /authorize:
    post:
      responses:
        '200':
          description: 200 OK
      tags:
        - Authorization
      summary: Obtain access token. You need an access token before you can start using the Likid APIs
      description: Obtain access token. You need an access token before you can start using the Likid APIs
      operationId: Authorize
      parameters:
        - name: client
          required: true
          in: body
          schema:
            $ref: '#/definitions/Client'
          description: Client credentials obtained at registration
  /commerce/products:
    get:
      responses:
        '200':
          description: 200 OK
      summary: List products
      tags:
        - Product
      operationId: ListProducts
    post:
      responses:
        '200':
          description: 200 OK
      operationId: AddProduct
      summary: Create new Product
      parameters:
        - name: product
          required: true
          in: body
          schema:
            $ref: '#/definitions/Product'
      tags:
        - Product
  '/commerce/products/{id}':
    get:
      responses:
        '200':
          description: 200 OK
      summary: Get a Product
      operationId: GetProduct
      tags:
        - Product
      parameters:
        - name: id
          type: string
          required: true
          in: path
    put:
      responses:
        '200':
          description: 200 OK
      summary: Update a Product
      operationId: UpdateProduct
      tags:
        - Product
      parameters:
        - name: id
          type: string
          required: true
          in: path
  /users:
    post:
      responses:
        '200':
          description: 200 OK
      tags:
        - User
      summary: Add a user
      operationId: AddUser
      parameters:
        - name: user
          required: true
          in: body
          schema:
            $ref: '#/definitions/User'
  /account:
    post:
      responses:
        '200':
          description: 200 OK
      summary: Join Network by creating a new account
      parameters:
        - name: account
          required: true
          in: body
          schema:
            $ref: '#/definitions/Account'
      tags:
        - Account
      operationId: CreateAccount
  /authenticate:
    post:
      responses:
        '200':
          description: 200 OK
      operationId: Authenticate
      summary: Authenticate a user
      parameters:
        - name: authenticate
          required: true
          in: body
          schema:
            $ref: '#/definitions/UsernamePasswordAuth'
      tags:
        - Authorization
  /credit/request:
    post:
      responses:
        '200':
          description: 200 OK
      operationId: CreditRequest
      summary: Request for Credit
      parameters:
        - name: credit
          required: true
          in: body
          schema:
            $ref: '#/definitions/Credit'
      tags:
        - Credit
  '/credit/profile/{profileId}':
    get:
      responses:
        '200':
          description: 200 OK
      tags:
        - Credit
      summary: Load a credit score card
      operationId: CreditScoreCard
      parameters:
        - name: profileId
          type: string
          required: true
          in: path
          description: A valid user's credit profile id
  '/users/verify/{userId}':
    put:
      responses:
        '200':
          description: 200 OK
      summary: Verify a user
      operationId: VerifyUser
      parameters:
        - name: userId
          type: string
          required: true
          in: path
      tags:
        - User
tags:
  - name: Credit
    description: Create and manage user credit request
  - name: User
    description: User management
  - name: Account
    description: Create a new Likid Account
  - name: Product
    description: APIs for Consumer product and Commodities
  - name: Bills Payment
    description: APIs for Utilities
  - name: Authorization
    description: Authorise your client applications to begin to create amazing digital solutions
  - name: Mortgage
    description: Managed mortgage assets
  - name: FX
    description: 'API for Atomic Transfer of currencies. Support for USD, EUR, GBP, NGN etc'
  - name: Collections
    description: 'Card Management Solution for Account Linking, Wallet-to-Card sync and POS'
  - name: Trade Finance
    description: Infrastructure for trade
  - name: Credit
    description: 'A fully AI enabled credit system for term loans, securitisation, credit scoring etc'
  - name: Wallet
    description: A secured private cold storage wallet for storing your identities.
  - name: Server Information
    description: Likid provides several endpoints to describe the server as well as handle various financial services.
  - name: Withdraw
    description: You can only withdraw what you own.
  - name: KYC
    description: Know your customer. Everything in Likid starts with KYC.
  - name: Transfers
    description: Transfer something you own to user A.
  - name: Funds
    description: Funds is a representation of physical assets you own.
definitions:
  Fund:
    properties:
      owner:
        type: string
        description: The userid that represents owner
      type:
        type: string
        description: The symbol of the fund. E.g USD
        example: USD
      symbol:
        type: string
        description: The number of units
        example: '10000'
      id:
        type: string
        description: Object id
    additionalProperties: false
    required:
      - owner
      - type
      - symbol
  Transfer:
    properties:
      to:
        type: string
        description: New owner details
      from:
        type: string
        description: Source owner of this asset
      quantity:
        type: string
        description: Number of units to be transferred
      id:
        type: string
        description: Object id
    additionalProperties: false
  Withdraw:
    properties:
      quantity:
        type: string
        description: How many units
      id:
        type: string
        description: Object id
    additionalProperties: false
    required:
      - quantity
  Kyc:
    properties:
      fullnames:
        type: string
        description: User fullnames
      address:
        type: string
        description: User verifiable address
      phoneNumber:
        type: string
        description: User phoneNumber
      country:
        type: string
        description: User country
      email:
        type: string
        description: User email
      identityId:
        type: string
      id:
        type: string
        description: Object id
    additionalProperties: false
    required:
      - fullnames
      - address
      - phoneNumber
      - country
      - email
      - identityId
  ResponseError:
    properties:
      message:
        type: string
      code:
        type: string
    additionalProperties: false
    required:
      - message
      - code
  Client:
    properties:
      client_id:
        type: string
        description: The client's client_id obtained at registration
      client_secret:
        type: string
        description: The client's client_secret obtained at registration
      username:
        type: string
        example: joe@example.com
        description: The logon username
      password:
        type: string
        description: The logon password
    additionalProperties: false
    required:
      - client_id
      - client_secret
      - username
      - password
  Wallet:
    properties:
      name:
        type: string
        description: The name of the wallet
      symbol:
        type: string
        description: The currency this wallet will hold
        example: USD
    additionalProperties: false
    required:
      - name
      - symbol
  Product:
    properties:
      name:
        type: string
        example: Puppy Cat
      price:
        type: number
        format: double
        example: 2
      description:
        type: string
      insurable:
        type: boolean
        description: Is the product insurable? Defaults to False
      bidable:
        type: boolean
        description: Is the product bidable? Defaults to False
      quantity:
        type: string
      iscontract:
        type: boolean
        description: opens to forward and futures. Defaults to False
      type:
        type: string
        example: ''
        description: Commodity or Consumer Product. Defaults to Consumer Product
    additionalProperties: false
    description: A Consumer Product or Commodity
    required:
      - name
      - price
      - description
      - quantity
  FX:
    properties:
      pair:
        type: string
        example: USD/NGN
      trade:
        type: string
        description: Buy or Sell
      quantity:
        type: number
        format: double
    additionalProperties: false
    description: Buy/Sell FX. Backed by respectable financial institutions.
    required:
      - pair
      - trade
      - quantity
  Account:
    properties:
      email:
        type: string
      accountName:
        type: string
      firstname:
        type: string
      lastname:
        type: string
      password:
        type: string
      country:
        type: string
      city:
        type: string
      address:
        type: string
    additionalProperties: false
    description: Join the Likid Network
    required:
      - address
      - city
      - country
      - password
      - lastname
      - firstname
      - accountName
      - email
  User:
    properties:
      email:
        type: string
      accountname:
        type: string
      password:
        type: string
      cellphone:
        type: string
      firstname:
        type: string
      lastname:
        type: string
      country:
        type: string
      language:
        type: string
      address:
        type: string
      profileurl:
        type: string
      appsprofile:
        type: string
      accesstoken:
        type: string
    additionalProperties: false
    required:
      - address
      - country
      - lastname
      - firstname
      - cellphone
      - password
      - email
    description: User Management
  Profile:
    properties:
      id:
        type: string
      accountName:
        type: string
      email:
        type: string
      cellphone:
        type: string
      country:
        type: string
      address:
        type: string
      firstname:
        type: string
      lastname:
        type: string
      city:
        type: string
      accesstoken:
        type: string
      profileurl:
        type: string
      language:
        type: string
      nickname:
        type: string
      dob:
        type: string
      occupation:
        type: string
      iscorporate:
        type: string
    additionalProperties: false
    description: A user profile
    required:
      - city
      - lastname
      - firstname
      - address
      - country
      - cellphone
      - email
  UsernamePasswordAuth:
    properties:
      email:
        type: string
      password:
        type: string
    additionalProperties: false
    required:
      - email
      - password
  Credit:
    properties:
      annualIncome:
        type: number
        description: Your current annual income
        format: double
      homeOwnership:
        type: boolean
        description: Do you have a home
      empLength:
        type: string
        description: Number of years in current employment
      loanAmt:
        type: integer
        description: The credit amount
        format: int32
      purpose:
        type: string
        description: The purpose for the credit
      desc:
        type: string
        description: Credit description
    additionalProperties: false
    required:
      - annualIncome
      - homeOwnership
      - empLength
      - loanAmt
      - purpose
      - desc
x-ibm-endpoints:
  - endpointUrl: 'https://api.eu-gb.apiconnect.appdomain.cloud/kayodeodeyemigreenwoodng-dev/sb'
    type:
      - production
      - development
