FROM node:8-alpine

WORKDIR /app
COPY . /app/

RUN npm install

CMD ["npm", "api-docs"]

# api2html -c ~/Downloads/web_NEW/likid_logo_4.svg -o api.html ~/Downloads/Brokers\ Provider\ API-1.0.0.yaml
